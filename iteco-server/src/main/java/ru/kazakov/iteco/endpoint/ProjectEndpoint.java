package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IProjectEndpoint;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.dto.ProjectDTO;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.enumeration.SortType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IProjectEndpoint")
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    private final IProjectService projectService;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.projectService = serviceLocator.getProjectService();
    }

    @Override
    @WebMethod
    public void mergeProject(
            @Nullable final String token,
            @Nullable final ProjectDTO dto
    ) throws Exception {
        validate(token);
        @Nullable final Project entity = serviceLocator.getDomainService().getProjectFromDTO(dto);
        projectService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistProject(
            @Nullable final String token,
            @Nullable final ProjectDTO dto
    ) throws Exception {
        validate(token);
        @Nullable final Project entity = serviceLocator.getDomainService().getProjectFromDTO(dto);
        projectService.persist(entity);
    }

    @Override
    @WebMethod
    public void createProject(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        projectService.create(token, name);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        projectService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllProjects(@Nullable final String token) throws Exception {
        validate(token);
        projectService.removeAll();
    }

    @Override
    @WebMethod
    public void removeAllProjectsByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        projectService.removeAll(currentId);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findByProjectNameCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        @Nullable final Project entity = projectService.findByName(currentId, name);
        return serviceLocator.getDomainService().getProjectDTO(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public ProjectDTO findOneProject(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        @Nullable final Project entity = projectService.findOne(id);
        return serviceLocator.getDomainService().getProjectDTO(entity);
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(@Nullable final String token) throws Exception {
        validate(token);
        List<Project> list = projectService.findAll();
        return list.stream().map(v -> serviceLocator.getDomainService().getProjectDTO(v)).collect(Collectors.toList());
    }

    @NotNull
    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjectsByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        List<Project> list = projectService.findAll(currentId);
        return list.stream().map(v -> serviceLocator.getDomainService().getProjectDTO(v)).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllSortedProjectsByCurrentId(
            @Nullable final String token,
            @Nullable final SortType sortType
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return projectService.findAll(currentId, sortType);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllProjectsByNameCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return projectService.findAllByName(currentId, part);
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllProjectsByInfoCurrentId(
            @Nullable final String token,
            @Nullable final String part
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return projectService.findAllByInfo(currentId, part);
    }

    @WebMethod
    public boolean containsProject(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        validate(token);
        return projectService.contains(name);
    }

    @Override
    @WebMethod
    public boolean containsProjectByCurrentId(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return projectService.contains(currentId, name);
    }

    @Override
    @WebMethod
    public boolean isEmptyProjectRepositoryByCurrentId(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        return projectService.isEmptyRepository(currentId);
    }

}
