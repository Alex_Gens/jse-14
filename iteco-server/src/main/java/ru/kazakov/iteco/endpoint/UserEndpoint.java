package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.UserDTO;
import ru.kazakov.iteco.entity.User;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.IUserEndpoint")
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.userService = serviceLocator.getUserService();
    }

    @NotNull
    private final IUserService userService;

    @Override
    @WebMethod
    public void mergeUser(
            @Nullable final String token,
            @Nullable final UserDTO dto
    ) throws Exception {
        validate(token);
        @Nullable final User entity = serviceLocator.getDomainService().getUserFromDTO(dto);
        userService.merge(entity);
    }

    @Override
    @WebMethod
    public void persistUser(@Nullable final UserDTO dto) throws Exception {
        @Nullable final User entity = serviceLocator.getDomainService().getUserFromDTO(dto);
        userService.persist(entity);
    }

    @Override
    @WebMethod
    public void removeUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        userService.remove(id);
    }

    @Override
    @WebMethod
    public void removeAllUsers(@Nullable final String token) throws Exception {
        validate(token);
        userService.removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findOneUser(
            @Nullable final String token,
            @Nullable final String id
    ) throws Exception {
        validate(token);
        @Nullable final User entity = userService.findOne(id);
        return serviceLocator.getDomainService().getUserDTO(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findUserByLogin(
            @Nullable final String token,
            @Nullable final String login
    ) throws Exception {
        validate(token);
        @Nullable final User entity = userService.findByLogin(login);
        return serviceLocator.getDomainService().getUserDTO(entity);
    }

    @Nullable
    @Override
    @WebMethod
    public UserDTO findCurrentUser(@Nullable final String token) throws Exception {
        @NotNull final String currentId = validateReturnCurrentId(token);
        @Nullable final User entity = userService.findOne(currentId);
        return serviceLocator.getDomainService().getUserDTO(entity);
    }

    @NotNull
    @Override
    @WebMethod
    public List<UserDTO> findAllUsers(@Nullable final String token) throws Exception {
        validate(token);
        List<User> list = userService.findAll();
        return list.stream().map(v -> serviceLocator.getDomainService().getUserDTO(v)).collect(Collectors.toList());
    }

    @Nullable
    @Override
    @WebMethod
    public List<String> findAllUsersLogins(@Nullable final String token) throws Exception {
        validate(token);
        return userService.findAllUsersLogins();
    }

    @Override
    @WebMethod
    public boolean containsUser(@Nullable final String login) throws Exception {
        return userService.contains(login);
    }

}
