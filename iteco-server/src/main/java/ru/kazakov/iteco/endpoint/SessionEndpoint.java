package ru.kazakov.iteco.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.ISessionEndpoint;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@WebService(endpointInterface = "ru.kazakov.iteco.api.endpoint.ISessionEndpoint")
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @NotNull
    private final ISessionService sessionService;

    public SessionEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
        this.sessionService = serviceLocator.getSessionService();
    }

    @NotNull
    @Override
    @WebMethod
    public RoleType getSessionRoleType(@Nullable final String token) throws Exception {
        return sessionService.getRoleType(token);
    }

    @Nullable
    @Override
    @WebMethod
    public String getInstanceToken(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        return sessionService.getInstance(login, password);
    }

    @Override
    @WebMethod
    public void persistSession(@Nullable final SessionDTO dto) throws Exception {
        @Nullable final Session entity = serviceLocator.getDomainService().getSessionFromDTO(dto);
        sessionService.persist(entity);}

    @Override
    @WebMethod
    public void removeSession(@Nullable final String id) throws Exception {
        sessionService.remove(id);
    }

    @Override
    @WebMethod
    public void removeSessionByUserId(@Nullable final String userId) throws Exception {
        sessionService.removeByUserId(userId);
    }

    @Override
    @WebMethod
    public void removeAllSessions() throws Exception {sessionService.removeAll();}

    @Nullable
    @Override
    @WebMethod
    public SessionDTO findOneSession(@Nullable final String id) throws Exception {
        @Nullable final Session entity = sessionService.findOne(id);
        return serviceLocator.getDomainService().getSessionDTO(entity);
    }

    @NotNull
    @Override
    @WebMethod
    public  List<SessionDTO> findAllSessions() throws Exception {
        List<Session> list = sessionService.findAll();
        return list.stream().map(v -> serviceLocator.getDomainService().getSessionDTO(v)).collect(Collectors.toList());
    }

    @Override
    @WebMethod
    public boolean containsSession(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        return sessionService.contains(userId, id);
    }

}
