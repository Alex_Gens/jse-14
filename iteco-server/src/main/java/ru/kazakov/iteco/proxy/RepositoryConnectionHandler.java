package ru.kazakov.iteco.proxy;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IRepository;
import javax.persistence.EntityManager;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class RepositoryConnectionHandler implements InvocationHandler {

       @NotNull
       private final IRepository repository;

       @NotNull
       private final EntityManager manager;

    public RepositoryConnectionHandler(@NotNull final IRepository repository,
                                       @NotNull final EntityManager manager) {
        this.repository = repository;
        this.manager = manager;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) {
        manager.getTransaction().begin();
        @Nullable Object result = null;
        try {
            result = method.invoke(repository, args);
            manager.getTransaction().commit();

        } catch (Exception e) {manager.getTransaction().rollback();}
        return result;
    }

}
