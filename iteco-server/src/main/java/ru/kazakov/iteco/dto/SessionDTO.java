package ru.kazakov.iteco.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
@NoArgsConstructor
public class SessionDTO extends AbstractDTO {

    @NotNull
    private String userId;

    private long timestamp = System.currentTimeMillis();

    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @Nullable
    private  String signature;

}
