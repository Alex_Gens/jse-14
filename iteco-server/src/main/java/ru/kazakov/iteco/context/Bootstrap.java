package ru.kazakov.iteco.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.api.service.*;
import ru.kazakov.iteco.endpoint.*;
import ru.kazakov.iteco.service.*;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public class Bootstrap implements ServiceLocator {

    @NotNull
    private final EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("ENTERPRISE");

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(this);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(this);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(this);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(this);

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(projectService, taskService, userService, sessionService);

    @Getter
    @NotNull
    private final EntityManagerService entityManagerService = new EntityManagerService(entityManagerFactory);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);

    public void start() {
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/ProjectEndpoint?wsdl", projectEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/TaskEndpoint?wsdl", taskEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/UserEndpoint?wsdl", userEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/SessionEndpoint?wsdl", sessionEndpoint);
        Endpoint.publish("http://" + propertyService.getHost() + ":"
                                          + propertyService.getPort()
                                          + "/DomainEndpoint?wsdl", domainEndpoint);
    }

}
