package ru.kazakov.iteco.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.RoleType;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task_manager.user")
public class User extends AbstractEntity  {

    @Column
    @Nullable
    private String name;

    @Column(unique = true)
    @Nullable
    private String login;

    @Column
    @Nullable
    private String password;

    @Column
    @NotNull
    private Date dateCreate = new Date();

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private RoleType roleType = RoleType.DEFAULT;

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Project> projects = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Task> tasks = new ArrayList<>();

    @Nullable
    @OneToMany(mappedBy = "user", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Session> sessions = new ArrayList<>();

}
