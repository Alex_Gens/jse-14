package ru.kazakov.iteco.entity;

import lombok.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.enumeration.Status;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "task_manager.project")
public class Project extends AbstractEntity {

    @NotNull
    @ManyToOne
    @JoinColumn(name = "userId", referencedColumnName = "id")
    private User user;

    @Column
    @Nullable
    private String name;

    @Column
    @NotNull
    private Date dateCreate = new Date();

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @NotNull
    @Enumerated(EnumType.STRING)
    private Status status = Status.PLANNED;

    @Column(name = "information")
    @Nullable
    private String info;

    @Nullable
    @OneToMany(mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private Collection<Task> tasks = new ArrayList<>();

    public boolean isEmpty() {return info == null || info.isEmpty();}

}
