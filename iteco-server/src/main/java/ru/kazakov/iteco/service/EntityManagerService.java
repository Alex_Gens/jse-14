package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public final class EntityManagerService {

    @NotNull
    private final EntityManagerFactory entityManagerFactory;


    public EntityManagerService(@NotNull final EntityManagerFactory entityManagerFactory) {
        this.entityManagerFactory = entityManagerFactory;
    }

    @NotNull
    public EntityManager getEntityManager() {return entityManagerFactory.createEntityManager();}

}
