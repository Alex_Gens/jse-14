package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.ISessionRepository;
import ru.kazakov.iteco.api.service.ISessionService;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.enumeration.RoleType;
import ru.kazakov.iteco.proxy.RepositoryConnectionHandler;
import ru.kazakov.iteco.repository.SessionRepository;
import ru.kazakov.iteco.util.AES;
import ru.kazakov.iteco.util.Password;
import ru.kazakov.iteco.util.SignatureUtil;
import javax.persistence.EntityManager;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public SessionService(@NotNull final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @NotNull
    @Override
    public RoleType getRoleType(@Nullable final String token) throws Exception {
        if (token == null || token.isEmpty()) throw new Exception();
        @NotNull final String secret = serviceLocator.getPropertyService().getSecret();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final Session session = mapper.readValue(decryptToken, Session.class);
        if (session == null) throw new Exception();
        return session.getRoleType();
    }

    @Override
    public void persist(@Nullable final Session entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.persistSession(entity);
        manager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeSession(id);
        manager.close();
    }

    @Override
    public void removeByUserId(@Nullable final String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeSessionByUserId(userId);
        manager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllSessions();
        manager.close();
    }

    @Nullable
    @Override
    public Session findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final Session session = proxyRepository.findOneSession(id);
        return proxyRepository.findOneSession(id);
    }

    @NotNull
    @Override
    public List<Session> findAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        List<Session> list = proxyRepository.findAllSessions();
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @Nullable
    @Override
    public String getInstance(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        if (password == null || password.isEmpty()) throw new Exception();
        @NotNull final IUserService userService = serviceLocator.getUserService();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new Exception();
        if (user.getPassword() == null || user.getPassword().isEmpty()) throw new Exception();
        @NotNull final String enteredPassword = Password.getHashedPassword(password);
        if (user.getPassword().equals(enteredPassword)) {
            user.setDateStart(new Date(System.currentTimeMillis()));
            userService.merge(user);
            Session userSession = new Session();
            userSession.setRoleType(user.getRoleType());
            userSession.setUser(user);
            @Nullable final SessionDTO dto = serviceLocator.getDomainService().getSessionDTO(userSession);
            if (dto == null) throw new Exception();
            @Nullable final String signature = SignatureUtil.getSignature(dto);
            if (signature == null || signature.isEmpty()) throw new Exception();
            dto.setSignature(signature);
            userSession.setSignature(signature);
            @NotNull final ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            @NotNull final String json = mapper.writeValueAsString(dto);
            @NotNull final String secret = serviceLocator.getPropertyService().getSecret();
            @NotNull final String token = AES.encrypt(json, secret);
            persist(userSession);
            return token;
        }
        return null;
    }

    @Override
    public boolean contains(
            @Nullable final String userId,
            @Nullable final String id
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new Exception();
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ISessionRepository repository = new SessionRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ISessionRepository proxyRepository = (ISessionRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsSession(userId, id);
        manager.close();
        return contains;
    }

}
