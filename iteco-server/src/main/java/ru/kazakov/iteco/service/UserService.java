package ru.kazakov.iteco.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.api.service.IUserService;
import ru.kazakov.iteco.entity.User;
import ru.kazakov.iteco.proxy.RepositoryConnectionHandler;
import ru.kazakov.iteco.repository.UserRepository;
import javax.persistence.EntityManager;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.List;

public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public UserService(@NotNull final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    public void persist(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.persistUser(entity);
        manager.close();
    }

    @Override
    public void merge(@Nullable final User entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.mergeUser(entity);
        manager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeUser(id);
        manager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllUsers();
        manager.close();
    }

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final User user = proxyRepository.findOneUser(id);
        manager.close();
        return user;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final User user = proxyRepository.findUserByLogin(login);
        manager.close();
        return user;
    }

    @Nullable
    @Override
    public User findCurrentUser(@Nullable final String currentId) throws Exception {
        if (currentId == null || currentId.isEmpty()) throw new Exception();
        return findOne(currentId);
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<User> list = proxyRepository.findAllUsers();
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllUsersLogins() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<String> list = proxyRepository.findAllUsersLogins();
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String login) throws Exception {
        if (login == null || login.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IUserRepository repository = new UserRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IUserRepository proxyRepository = (IUserRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsUserByLogin(login);
        manager.close();
        return contains;
    }

}
