package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.ITaskRepository;
import ru.kazakov.iteco.api.service.ITaskService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.entity.Task;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.proxy.RepositoryConnectionHandler;
import ru.kazakov.iteco.repository.TaskRepository;
import ru.kazakov.iteco.util.AES;
import javax.persistence.EntityManager;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.List;

public class TaskService extends AbstractService<Task> implements ITaskService {
    
    @NotNull
    private final ServiceLocator serviceLocator;

    public TaskService(@NotNull final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    public void persist(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.persistTask(entity);
        manager.close();
    }

    @Override
    public void merge(@Nullable final Task entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.mergeTask(entity);
        manager.close();
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeTask(id);
        manager.close();
    }

    @Override
    public void create(@Nullable final String token,
                       @Nullable final String name
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final String secret = serviceLocator.getPropertyService().getSecret();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final SessionDTO dto = mapper.readValue(decryptToken, SessionDTO.class);
        if (dto == null) throw new Exception();
        @Nullable final Session session = serviceLocator.getDomainService().getSessionFromDTO(dto);
        if (session == null) throw new Exception();
        @NotNull final Task task = new Task();
        task.setUser(session.getUser());
        task.setName(name);
        persist(task);
    }

    @Override
    public void removeWithProject(
            @Nullable final String currentUserId,
            @Nullable final String projectId
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeTasksWithProject(currentUserId, projectId);
        manager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllTasks();
        manager.close();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllTasksByCurrentId(currentUserId);
        manager.close();
    }

    @Override
    public void removeAllWithProjects(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllTasksWithProjects(currentUserId);
        manager.close();
    }

    @Nullable
    @Override
    public Task findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final Task task = proxyRepository.findOneTask(id);
        manager.close();
        return task;
    }

    @Nullable
    @Override
    public Task findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final Task task = proxyRepository.findTaskByNameCurrentId(currentUserId, name);
        manager.close();
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<Task> list = proxyRepository.findAllTasks();
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<Task> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<Task> list = proxyRepository.findAllTasksByCurrentId(currentUserId);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<String> list = proxyRepository.findAllSortedTasksByCurrentId(currentUserId, sortType);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final String projectId,
            @Nullable final SortType sortType) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (projectId == null || projectId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<String> list = proxyRepository.findAllSortedTasksByCurrentIdProjectId(
                currentUserId, projectId, sortType);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<String> list = proxyRepository.findAllTasksByNameCurrentId(currentUserId, part);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @NotNull final List<String> list = proxyRepository.findAllTasksByInfoCurrentId(currentUserId, part);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsTaskByName(name);
        manager.close();
        return contains;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsTaskByNameCurrentId(currentUserId, name);
        manager.close();
        return contains;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ITaskRepository repository = new TaskRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final ITaskRepository proxyRepository = (ITaskRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean isEmpty = proxyRepository.isEmptyTaskRepository(currentUserId);
        manager.close();
        return isEmpty;
    }

}
