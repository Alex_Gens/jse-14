package ru.kazakov.iteco.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.repository.IProjectRepository;
import ru.kazakov.iteco.api.service.IProjectService;
import ru.kazakov.iteco.dto.SessionDTO;
import ru.kazakov.iteco.entity.Project;
import ru.kazakov.iteco.entity.Session;
import ru.kazakov.iteco.enumeration.SortType;
import ru.kazakov.iteco.proxy.RepositoryConnectionHandler;
import ru.kazakov.iteco.repository.ProjectRepository;
import ru.kazakov.iteco.util.AES;
import javax.persistence.EntityManager;
import java.lang.reflect.Proxy;
import java.util.Collections;
import java.util.List;

public class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    private final ServiceLocator serviceLocator;

    public ProjectService(@NotNull final ServiceLocator serviceLocator) {this.serviceLocator = serviceLocator;}

    @Override
    public void persist(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.persistProject(entity);
        manager.close();
    }

    @Override
    public void merge(@Nullable final Project entity) throws Exception {
        if (entity == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.mergeProject(entity);
        manager.close();
    }

    @Override
    public void create(
            @Nullable final String token,
            @Nullable final String name
    ) throws Exception {
        if (token == null || token.isEmpty()) throw new Exception();
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final String secret = serviceLocator.getPropertyService().getSecret();
        @NotNull final String decryptToken = AES.decrypt(token, secret);
        @NotNull final ObjectMapper mapper = new ObjectMapper()
                .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        @Nullable final SessionDTO dto = mapper.readValue(decryptToken, SessionDTO.class);
        if (dto == null) throw new Exception();
        @Nullable final Session session = serviceLocator.getDomainService().getSessionFromDTO(dto);
        if (session == null) throw new Exception();
        @NotNull final Project project = new Project();
        project.setUser(session.getUser());
        project.setName(name);
        persist(project);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeProject(id);
        manager.close();
    }

    @Override
    public void removeAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllProjects();
        manager.close();
    }

    @Override
    public void removeAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        proxyRepository.removeAllProjectsByCurrentId(currentUserId);
        manager.close();
    }

    @Nullable
    @Override
    public Project findOne(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final Project project = proxyRepository.findOneProject(id);
        manager.close();
        return project;
    }

    @Nullable
    @Override
    public Project findByName(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final Project project = proxyRepository.findByProjectNameCurrentId(currentUserId, name);
        manager.close();
        return project;
    }

    @NotNull
    @Override
    public List<Project> findAll() throws Exception {
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<Project> list = proxyRepository.findAllProjects();
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<Project> findAll(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<Project> list = proxyRepository.findAllProjectsByCurrentId(currentUserId);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAll(
            @Nullable final String currentUserId,
            @Nullable final SortType sortType
    ) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        if (sortType == null) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final ProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<String> list = proxyRepository.findAllSortedProjectsByCurrentId(currentUserId, sortType);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByName(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<String> list = proxyRepository.findAllProjectsByNameCurrentId(currentUserId, part);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @NotNull
    @Override
    public List<String> findAllByInfo(
            @Nullable final String currentUserId,
            @Nullable final String part
    ) throws Exception {
        if (part == null || part.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        @Nullable final List<String> list = proxyRepository.findAllProjectsByInfoCurrentId(currentUserId, part);
        manager.close();
        return list == null ? Collections.emptyList() : list;
    }

    @Override
    public boolean contains(@Nullable final String name) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsProjectByName(name);
        manager.close();
        return contains;
    }

    @Override
    public boolean contains(
            @Nullable final String currentUserId,
            @Nullable final String name
    ) throws Exception {
        if (name == null || name.isEmpty()) throw new Exception();
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean contains = proxyRepository.containsProjectByNameCurrentId(currentUserId, name);
        manager.close();
        return contains;
    }

    @Override
    public boolean isEmptyRepository(@Nullable final String currentUserId) throws Exception {
        if (currentUserId == null || currentUserId.isEmpty()) throw new Exception();
        @NotNull final EntityManager manager = serviceLocator.getEntityManagerService().getEntityManager();
        @NotNull final IProjectRepository repository = new ProjectRepository(manager);
        @NotNull final ClassLoader loader = repository.getClass().getClassLoader();
        @NotNull final Class[] interfaces = repository.getClass().getInterfaces();
        @NotNull final IProjectRepository proxyRepository = (IProjectRepository) Proxy.newProxyInstance(
                loader, interfaces, new RepositoryConnectionHandler(repository, manager));
        final boolean isEmpty = proxyRepository.isEmptyProjectRepository(currentUserId);
        manager.close();
        return isEmpty;
    }

}
