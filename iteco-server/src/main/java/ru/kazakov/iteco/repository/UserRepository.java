package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.repository.IUserRepository;
import ru.kazakov.iteco.entity.User;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;
import static ru.kazakov.iteco.constant.Constant.*;

public class UserRepository extends AbstractRepository implements IUserRepository {

    public UserRepository(@NotNull final EntityManager entityManager) {super(entityManager);}

    @Override
    public void persistUser(@NotNull final User entity) {entityManager.persist(entity);}

    @Override
    public void mergeUser(@NotNull final User entity) {entityManager.merge(entity);}

    @Override
    public void removeUser(@NotNull final String id) {
        @NotNull final String jpql = String.format("delete from User u where u.%s = :%s", ID, ID);
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.setParameter(ID, id);
        query.executeUpdate();
    }

    @Override
    public void removeAllUsers() {
        @NotNull final String jpql = "delete from User u";
        @NotNull final Query query = entityManager.createQuery(jpql);
        query.executeUpdate();
    }

    @Nullable
    @Override
    public User findOneUser(@NotNull final String id) {
        @NotNull final String jpql = String.format("select u from User u where u.%s = :%s", ID, ID);
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        query.setParameter(ID, id);
        @Nullable final User user = query.getSingleResult();
        return user;
    }

    @Nullable
    @Override
    public User findUserByLogin(@NotNull final String login) {
        @NotNull final String jpql = String.format("select u from User u " +
                "where u.%s = :%s", LOGIN, LOGIN);
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        query.setParameter(LOGIN, login);
        @Nullable final User user = query.getSingleResult();
        return user;
    }

    @NotNull
    @Override
    public List<User> findAllUsers() {
        @NotNull final String jpql = "select u from User u";
        @NotNull final TypedQuery<User> query = entityManager.createQuery(jpql, User.class);
        @Nullable final List<User> users =  query.getResultList();
        return users;
    }

    @NotNull
    @Override
    public List<String> findAllUsersLogins() {
        @NotNull final String jpql = String.format("select u.%s from User u", LOGIN);
        @NotNull final TypedQuery<String> query = entityManager.createQuery(jpql, String.class);
        @Nullable final List<String> logins =  query.getResultList();
        return logins;
    }

    @Override
    public boolean containsUserByLogin(
            @NotNull final String login) {
        @NotNull final String jpql = String.format("select case when " +
                "(select u.%s from User u where u.%s = :%s) is not null " +
                "then true else false end from User u", ID, LOGIN, LOGIN);
        @NotNull final TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
        query.setParameter(LOGIN, login);
        final boolean contains = query.getResultList().stream().findFirst().orElse(false);
        return contains;
    }

}
