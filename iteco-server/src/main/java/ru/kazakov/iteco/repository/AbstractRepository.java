package ru.kazakov.iteco.repository;

import org.jetbrains.annotations.NotNull;
import javax.persistence.EntityManager;

public class AbstractRepository {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

}
