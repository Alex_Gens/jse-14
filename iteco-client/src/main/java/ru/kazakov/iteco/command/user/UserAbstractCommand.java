package ru.kazakov.iteco.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.kazakov.iteco.api.context.ServiceLocator;
import ru.kazakov.iteco.api.endpoint.IUserEndpoint;
import ru.kazakov.iteco.api.endpoint.RoleType;
import ru.kazakov.iteco.command.AbstractCommand;

@NoArgsConstructor
public abstract class UserAbstractCommand extends AbstractCommand {

    @Nullable
    protected IUserEndpoint userEndpoint;

    @NotNull
    private RoleType roleType = RoleType.DEFAULT;

    @NotNull
    @Override
    public RoleType getRoleType() {return this.roleType;}

    @Override
    public void setServiceLocator(@NotNull final ServiceLocator serviceLocator) {
        super.setServiceLocator(serviceLocator);
        this.userEndpoint = serviceLocator.getUserEndpoint();
    }

}
