package ru.kazakov.iteco.api.context;

import org.jetbrains.annotations.NotNull;
import ru.kazakov.iteco.api.endpoint.*;
import ru.kazakov.iteco.api.service.*;

public interface ServiceLocator {

    @NotNull
    public ITaskEndpoint getTaskEndpoint();

    @NotNull
    public IProjectEndpoint getProjectEndpoint();

    @NotNull
    public IUserEndpoint getUserEndpoint();

    @NotNull
    public ITerminalService getTerminalService();

    @NotNull
    public IDomainEndpoint getDomainEndpoint();

    @NotNull
    public ISessionEndpoint getSessionEndpoint();

}
